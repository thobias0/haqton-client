var jwtoken = "eyJhbGciOiJSUzI1NiJ9.eyJleHAiOjE2MDU5MzQ1NDEsImlhdCI6MTYwMjA0NjU0MSwiaXNzIjoiUW1vYiBTb2x1dGlvbnMiLCJ1c2VyIjp7InVzZXJuYW1lIjoidGhvYmlhczBAZ21haWwuY29tIn19.NgcAwWXAmsu-Qf_AmXwZe5u2SeCIb3YIKdPPFYsR4QCHeytKN9m2o_MoEODyffp62HkkqAXcIX9gXhAP5OpCX__4nRbw-d9WZxkeLKoC-DCuLlnbpflzF76agfrAMW6EK8QideXA7c-RzRh5qT925IE32toYnK-9l6qkdzlUvJJpXPXHiP0ZP_AIJoiKx3Q-cTeLTS9xkrmgcJtpv9l1th6b2JGuKNBLBAR4HiDMfFC1nvAPWYnrqgW_F5hDk6xUml4aWIy5W3R_zznZ0B6Y-qz3oL7IkJACYgM6PnvYXWwkScwdWXHIQYHqPSK9fIOYnlnCmVyIjqtILSB5zudtTzMxKTsF0CaWAMYM-KJv3joWk2o7mwux20ZiWlDcHjNmx33bFG-n3W0B2YLH_SKKPkwHrpxkzmZ9HF5LaQJ2EpFWUtQlFqdGSHm8Bny9WnyvPZR8z7mZ1pUcOwTHNAS_Tcf4DhwvH6SWPszs6T1JGBTk4x4kiCk_XPy3211fi6sNvz4rVbTxcbMP03vO-Xpn6mw3qvcoF2HEdlo7WZ_CgALtvUvqnzxjIZRwfVFrm-8oNwZJikO8qH1AQaEEPTnVuYMZY4lClaxKyFxENLdq_HdrEQNQ-JxqExkmMy6Cqgj-dTVSCN5lS6F1vWzV69JeZmyns847fQidX9b-20ZpTuI"

function create_match(value, callback) {
    var xhr = new XMLHttpRequest()
    xhr.open('POST', 'https://haqton.qmob.solutions/matches', true)
    xhr.setRequestHeader("Content-type", "application/json")
    xhr.setRequestHeader('Authorization', 'Bearer ' + jwtoken);


    xhr.onreadystatechange = (function(payload) {
        return function() {
            let response = payload.responseText
            let data = response.replace(/\r ? \n/, "")

            if(xhr.readyState === xhr.DONE && xhr.status === 200) {
                let json = JSON.parse(data)
                callback(json) //Success return
            }
        }
    })(xhr)

    xhr.send(value);
}

function matches_waiting_for_players(callback) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', 'https://haqton.qmob.solutions/matches', true)
    xhr.setRequestHeader("Content-type", "application/json")
    xhr.setRequestHeader('Authorization', 'Bearer ' + jwtoken);

    xhr.onreadystatechange = (function (payload) {

        return function() {
            let response = payload.responseText
            let data = response.replace(/\r ? \n/, "")

            if(xhr.readyState === xhr.DONE && xhr.status === 200) {
                let json = JSON.parse(data)
                callback(json)
            }
        }

    })(xhr)

    xhr.send("")
}

function enter_match(id, value, callback) {
    var xhr = new XMLHttpRequest()
    xhr.open('POST', 'https://haqton.qmob.solutions/matches/' + id + '/players', true)
    xhr.setRequestHeader("Content-type", "application/json")
    xhr.setRequestHeader('Authorization', 'Bearer ' + jwtoken);


    xhr.onreadystatechange = (function(payload) {
        return function() {
            let response = payload.responseText
            let data = response.replace(/\r ? \n/, "")

            if(xhr.readyState === xhr.DONE && xhr.status === 200) {
                let json = JSON.parse(data)
                callback(json) //Success return
            }
        }
    })(xhr)

    xhr.send(value);
}

function update_player(match_id, player_id, value, callback) {
    var xhr = new XMLHttpRequest()
    console.log('URL')
    var url = 'https://haqton.qmob.solutions/matches/' + match_id + '/players/' + player_id
    console.log(url)
    xhr.open('PUT', url, true)
    xhr.setRequestHeader("Content-type", "application/json")
    xhr.setRequestHeader('Authorization', 'Bearer ' + jwtoken);


    xhr.onreadystatechange = (function(payload) {
        return function() {
            let response = payload.responseText
            let data = response.replace(/\r ? \n/, "")

            if(xhr.readyState === xhr.DONE && xhr.status === 200) {
                let json = JSON.parse(data)
                callback(json) //Success return
            }
        }
    })(xhr)

    xhr.send(value);
}

//https://haqton.qmob.solutions/matches/1
function update_match(match_id, value, callback) {
    var xhr = new XMLHttpRequest()
    console.log('URL')
    var url = 'https://haqton.qmob.solutions/matches/' + match_id
    console.log(url)
    xhr.open('PUT', url, true)
    xhr.setRequestHeader("Content-type", "application/json")
    xhr.setRequestHeader('Authorization', 'Bearer ' + jwtoken);


    xhr.onreadystatechange = (function(payload) {
        return function() {
            let response = payload.responseText
            let data = response.replace(/\r ? \n/, "")

            if(xhr.readyState === xhr.DONE && xhr.status === 200) {
                let json = JSON.parse(data)
                callback(json) //Success return
            } else {
                console.log("from PUT: "+response)
            }
        }
    })(xhr)

    xhr.send(value);
}


function get_random_question(match_id, callback) {
    //https://haqton.qmob.solutions/matches/1/random_question
    var xhr = new XMLHttpRequest();

    var url = 'https://haqton.qmob.solutions/matches/' + match_id + '/random_question'
    xhr.open('GET', url, true)
    xhr.setRequestHeader("Content-type", "application/json")
    xhr.setRequestHeader('Authorization', 'Bearer ' + jwtoken);

    xhr.onreadystatechange = (function (payload) {

        return function() {
            let response = payload.responseText
            let data = response.replace(/\r ? \n/, "")

            if(xhr.readyState === xhr.DONE && xhr.status === 200) {
                let json = JSON.parse(data)
                callback(json)
            } else {
                console.log("from GET: "+response)
            }
        }

    })(xhr)

    xhr.send("")
}
