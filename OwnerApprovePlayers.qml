import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls.Material 2.15
import solutions.qmob.haqton 1.0

import "FontAwesome"

import "apiRest.js" as API

Item {
    id: rootId

    //Strings que contem o valor da classe c++
    property string players: Core.players
    property string myMatch: Core.myMatch

    //Habilitar o botao de inicio de partida
    property bool enableStartMatch: false

    onPlayersChanged: {
        console.log("ATUALIZAR VALORES DOS PLAYERS")
        console.log(JSON.stringify(players))

        playersListModelId.clear()
        var obj = JSON.parse(players)

        let mApproveCounter = 0

        Object.keys(obj).forEach(function(key) {
            playersListModelId.append(obj[key])
            if(obj[key].approved) {
                console.log("..:: Player: "+obj[key].player_name+" aprovado ::..")
                mApproveCounter++
            }
        })
        //Se o numero de jogadores com status aprovado for igual ao numero de jogadores
        //e o numero de players for maior que 1
        if(mApproveCounter === playersListModelId.count && playersListModelId.count > 1) {
            console.log("..:: ENABLED ::..")
            enableStartMatch = true
        } else {
            console.log("..:: DISABLED ::..")
            enableStartMatch = false
        }
        console.log("playersListModelId.count: "+playersListModelId.count)
    }

    onMyMatchChanged: {
        console.log("ATUALIZANDO VALORES DA MY MATCH")
        //console.log("matchesListModelId: "+matchesListModelId.length)
        matchListModelId.clear()
        var obj = JSON.parse(myMatch)
        matchListModelId.append(obj)
        console.log("matchListModelId.count: "+matchListModelId.count)
    }

    function update_player(match_id, player_id, approve, score) {
        var obj = {}
        obj.approved = approve
        obj.score = score

        API.update_player(match_id, player_id, JSON.stringify(obj), callback => {
                              console.log(JSON.stringify(callback))

                          })
    }

    Rectangle {
        id: containerRectId
        width: parent.width; height: parent.height
        color: "#44a8e4"
        radius: 15

        SequentialAnimation on color {
            loops: Animation.Infinite
            ColorAnimation { to: "#44a8e4"; duration: 5000 } ColorAnimation { to: "#8bc740"; duration: 5000 }
            ColorAnimation { to: "#f3d034"; duration: 5000 } ColorAnimation { to: "#f79154"; duration: 5000 }
            ColorAnimation { to: "#f3d034"; duration: 5000 } ColorAnimation { to: "#8bc740"; duration: 5000 }
        }

        ListView {
            id: listViewId
            anchors.top: parent.top
            anchors.topMargin: 8
            anchors.left: parent.left
            anchors.leftMargin: 8
            anchors.right: parent.right
            anchors.rightMargin: 8
            anchors.bottom: buttonsContainerId.top
            anchors.bottomMargin: 8
            model: playersListModelId
            clip: true
            spacing: 5
            headerPositioning: ListView.OverlayHeader
            footerPositioning: ListView.OverlayFooter

            delegate: Rectangle {
                id: rectId
                width: listViewId.width
                height: 110
                color: containerRectId.color
                border.color: "white"
                radius: 5

                ColumnLayout {
                    anchors.top: parent.top
                    anchors.left: parent.left
                    anchors.bottom: parent.bottom
                    width: parent.width/2
                    anchors.margins: 8

                    RowLayout {
                        Layout.fillHeight: true
                        Text {
                            Layout.fillHeight: true
                            text: "NOME: "
                            color: "white"
                        }

                        Text {
                            Layout.fillHeight: true
                            text: player_name
                            color: "white"
                        }
                    }

                    RowLayout {
                        Layout.fillHeight: true
                        Text {
                            Layout.fillHeight: true
                            text: "STATUS: "
                            color: "white"
                        }

                        Text {
                            Layout.fillHeight: true
                            text: approved ? "APROVADO" : "AGUARDANDO"
                            color: "white"
                        }
                    }

                    RowLayout {
                        Layout.fillHeight: true
                        Text {
                            Layout.fillHeight: true
                            text: "E-MAIL: "
                            color: "white"
                        }

                        Text {
                            Layout.fillHeight: true
                            text: player_email
                            color: "white"
                        }
                    }
                }

                Rectangle {
                    anchors.right: parent.right
                    anchors.rightMargin: 8
                    anchors.top: parent.top
                    anchors.topMargin: 8
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: 8
                    color: containerRectId.color
                    width: disapproveRectId.implicitWidth

                    Button {
                        id: approveRectId
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.top: parent.top
                        width: disapproveRectId.width
                        text: "APROVAR"
                        visible: !approved ? true : false

                        onClicked: {
                            playersListModelId.setProperty(index, "approved", true)
                            update_player(matchListModelId.get(0).id, id, approved, score)
                        }
                    }

                    Button {
                        id: disapproveRectId
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.bottom: parent.bottom
                        text: "DESAPROVAR"
                        visible: !approved ? true : false

                        onClicked: {
                            playersListModelId.setProperty(index, "approved", false)
                            update_player(matchListModelId.get(0).id, id, approved, score)
                        }
                    }
                }

            }

            header: Rectangle {
                id: playersListTextContainerId
                width: parent.width
                height: 50
                color: containerRectId.color
                z: 3

                Label {
                    anchors.centerIn: parent
                    horizontalAlignment: Text.AlignHCenter
                    id: matchNameTextId
                    font { family: gameFont.name; pixelSize: 32 }
                    color: "white"; text: "JOGADORES"
                    height: 50
                }

                MouseArea {
                    anchors.fill: parent

                    onClicked: {

                    }
                }
            }

            ScrollBar.vertical: ScrollBar {}
        }

        Rectangle {
            id: buttonsContainerId
            anchors.bottom: parent.bottom
            width: parent.width
            height: 50
            color: containerRectId.color
            z: 3

            RowLayout {
                anchors.fill: parent

                Button {
                    Layout.fillWidth: true
                    Layout.preferredWidth: 50
                    text: "VOLTAR"
                    leftInset: 10; bottomInset: 10
                    onClicked:  {
                        stackViewId.pop()
                    }
                }

                Button {
                    Layout.fillWidth: true
                    Layout.preferredWidth: 50
                    text: "INICIAR PARTIDA"
                    enabled: enableStartMatch
                    rightInset: 10; bottomInset: 10

                    onClicked: {
                        console.log("Match ID: "+matchListModelId.get(0).id)
                        let match_id = matchListModelId.get(0).id

                        //Seta o status da partida como 1 -> Rodando
                        let status_match_running_data = {"status": 1}
                        API.update_match(match_id, JSON.stringify(status_match_running_data), callback => {
                                            console.log(JSON.stringify(callback))

                                             //Antes de receber a questao, deve-se atualizar o status da partida para rodando
                                             API.get_random_question(match_id, callback => {
                                                                         console.log(JSON.stringify(callback))
                                                                     })
                                         })


                    }
                }
            }
        }
    }

    ListModel {
        id: playersListModelId
    }

    ListModel {
        id: matchListModelId
    }
}
