/***************************************************************************
**
**  Copyright (C) 2020 by Qmob Solutions <contato@qmob.solutions>
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Foobar. If not, see <https://www.gnu.org/licenses/>.
**
***************************************************************************/

#include "core.h"

#include <QDebug>
#include "messagingcontroller.h"

Q_LOGGING_CATEGORY(core, "solutions.qmob.haqton.core")

Core *Core::_instance = nullptr;

Core::Core(QObject *parent)
    : QObject(parent),
      _messagingController(new MessagingController(this)),
      m_message(""),
      m_players(""),
      m_myMatch("")
{
    connect(_messagingController, &MessagingController::newMessage,
            this, &Core::setMessage);

    /*connect(&_zeroMQSubscriberThread, &ZeroMQSubscriberThread::newMessage,
            this, &MessagingController::newMessage);
            */
}

Core::~Core()
{
    delete _messagingController;
}

Core *Core::instance()
{
    if (!_instance) {
        _instance = new Core;
    }
    return _instance;
}

MessagingController *Core::messagingController() const
{
    return _messagingController;
}

void Core::setTopic(QString topic)
{
    messagingController()->setTopic(topic);
}

void Core::setNewPlayers(QString obj)
{
    Core::setPlayers(obj);
}

void Core::setMyNewMatch(QString obj)
{
    Core::setMyMatch(obj);
}

QString Core::message() const
{
    return m_message;
}

void Core::setMessage(QString message)
{
    if (m_message == message)
        return;

    m_message = message;
    emit messageChanged(m_message);
}

QString Core::players() const
{
    return m_players;
}

void Core::setPlayers(QString players)
{
    if (m_players == players)
        return;

    m_players = players;
    emit playersChanged(m_players);
}

QString Core::myMatch() const
{
    return m_myMatch;
}

void Core::setMyMatch(QString myMatch)
{
    if (m_myMatch == myMatch)
        return;

    m_myMatch = myMatch;
    emit myMatchChanged(m_myMatch);
}
