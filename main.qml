/***************************************************************************
**
**  Copyright (C) 2020 by Qmob Solutions <contato@qmob.solutions>
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Foobar. If not, see <https://www.gnu.org/licenses/>.
**
***************************************************************************/

import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtMultimedia 5.15
import QtQuick.Controls.Material 2.15

import solutions.qmob.haqton 1.0

import "FontAwesome"

import "apiRest.js" as API

ApplicationWindow {
    id: rootId

    //GameButton IDs
    property int mCreateMatch: 0
    property int mFindMatch: 1
    property int mAbout: 2
    property int mSponsors: 3

    //Nickname and e-mail
    property string mNickName: ""
    property string mEmail: ""
    property int mId: -1

    //Matchname
    property string mMatchName: ""

    //Property with messages from ZMQ
    property string newMessage: Core.message

    //When received a message
    onNewMessageChanged: {
        console.log("..::New message arrived::..")
        var obj = JSON.parse(newMessage)

        if(obj.message === "players_update") {
            console.log(newMessage)
            //Atualizar lista de jogadores
            console.log("..::Atualizando lista de jgoadores::..")
            //Limpa a lista
            playersListModelId.clear()

            //Itera sobre o vetor de dados
            obj.data.forEach(function(item) {
                //Atualiza a lista com os dados recebidos pelo ZMQ
                var data = {}
                data.id = item.id
                data.player_name = item.player_name
                data.player_email = item.player_email
                data.approved = item.approved
                data.score = item.score
                data.match_id = item.match_id

                if(data.player_name === mNickName) {
                    mId = data.id
                    console.log("O id do player: "+mNickName+" eh: "+mId)
                }

                console.log("Entrou: "+data.player_name)

                //Adiciona o objeto a lista de jogador

                playersListModelId.append(data)
            })

            //Seta uma nova string com os jogadores atuais
            Core.setNewPlayers(get_players_data())

        } else if(obj.message === "matches_update") {
            console.log("..::Atualizando lista de partidas::..")
            //Atualizar a lista de partidas

            //Limpa lista
            //matchesListModelId.clear()

            //Varre elemento data que contem dados das partidas
            obj.data.forEach(function(item) {
                //Atualiza a lista com os dados recebidos pelo ZMQ
                var data = {}
                data.id = item.id
                var exist = false
                //Percorrer matchesListModelId
                for(let i = 0; i <matchesListModelId.count; i++) {
                    if(data.id === matchesListModelId.get(i).id) { //Caso o id da partida ja esteja no listmodel
                        exist = true //Seta flag como existe
                    }
                }

                if(!exist) {
                    data.description = item.description
                    data.owner = item.creator_name
                    data.status = item.status
                    data.topic = item.topic
                    data.number_of_players = item.match_players.length

                    //console.log(data.description)
                    //console.log(data.owner)

                    if(data.description.length !== 0) { //So adiciona partidas com descricao
                        matchesListModelId.append(data)
                    }
                }
            })
        }

    }

    function create_match(data) {
        API.create_match(JSON.stringify(data), callback => {
                             if(callback !== null) {
                                 console.log("..::Create Match Callback::..")
                                 var data = {}
                                 data.id = callback.id
                                 data.description = callback.description
                                 data.creator_name = callback.creator_name
                                 data.creator_email = callback.creator_email
                                 data.status = callback.status
                                 data.topic = callback.topic

                                 //Adiciona dados da partida a lista mMatchListModelId
                                 mMatchListModelId.append(data)

                                 //Adiciona dados do jogador a lista playersListModelId
                                 callback.match_players.forEach(function(item) {
                                     var data = {}
                                     data.id = item.id
                                     data.player_name = item.player_name
                                     data.player_email = item.player_email
                                     data.approved = item.approved
                                     data.score = item.score
                                     data.match_id = item.match_id

                                     console.log("Entrou: "+data.player_name)

                                     playersListModelId.append(data)
                                 })

                                 console.log("..::Tamanho de mMatchListModelId: "+mMatchListModelId.count)

                                 //Seta o novo objeto de jogadores para a classe c++
                                 Core.setNewPlayers(get_players_data())

                                 Core.setMyNewMatch(get_my_match_data())

                                 //Seta o topico do ZMQ para o topico da partida
                                 Core.setTopic(data.topic)

                                 //Empilha tela de aprovacao de jogadores pelo dono
                                 stackViewId.push(ownerApprovePlayersId);
                             }
                         })
    }

    function get_matches_waiting_for_players() {
        API.matches_waiting_for_players(callback => {
                                            //console.log(callback)
                                            //console.log(JSON.stringify(callback))
                                            callback.forEach(function(item) {

                                                var data = {}
                                                data.id = item.id
                                                data.description = item.description
                                                data.owner = item.creator_name
                                                data.status = item.status
                                                data.topic = item.topic
                                                data.number_of_players = item.match_players.length

                                                if(data.description.length !== 0) { //So adiciona partidas com descricao
                                                    matchesListModelId.append(data)
                                                }
                                            })
                                        })
    }

    function match_status_text(value) {
        var return_value = ""
        if(value === 0) {
            return_value = "AGUARDANDO JOGADORES"
        } else if(value === 1) {
            return_value = "EM ANDAMENTO"
        } else if(value === 2) {
            return_value = "FINALIZADA"
        }

        return return_value
    }

    function enter_match(id) {
        var data = {}
        data.nickname = mNickName
        data.email = mEmail
        API.enter_match(id, JSON.stringify(data), callback => {
                            console.log("\n\n")
                            console.log("..::enter match callback::..")
                            var data = {}
                            //console.log(callback)
                            callback.forEach(function(json) {
                                data.id = json.id
                                data.player_name = json.player_name
                                data.player_email = json.player_email
                                data.approved = json.approved
                                data.score = json.score
                                data.match_id = json.match_id
                            })
                            //console.log(JSON.stringify(callback))
                            console.log("..::finish match callback::..")
                            console.log("\n\n")
                        })
    }

    function get_my_match_data() {
        var data = {}
        data.id = mMatchListModelId.get(0).id
        data.description = mMatchListModelId.get(0).description
        data.creator_name = mMatchListModelId.get(0).creator_name
        data.creator_email = mMatchListModelId.get(0).creator_email
        data.status = mMatchListModelId.get(0).status
        data.topic = mMatchListModelId.get(0).topic
        //data.match_players = mMatchListModelId.get(0).match_players

        return JSON.stringify(data)
    }

    function get_players_data() {
        var data = {}

        for(let i = 0; i < playersListModelId.count; i++) {
            let player = "player_"+ (i)
            data[player] = {}
            data[player].id = playersListModelId.get(i).id
            data[player].player_name = playersListModelId.get(i).player_name
            data[player].player_email = playersListModelId.get(i).player_email
            data[player].approved = playersListModelId.get(i).approved
            data[player].score = playersListModelId.get(i).score
            data[player].match_id = playersListModelId.get(i).match_id
        }

        //        console.log("..:: playersListCount: "+ playersListModelId.count + " ::..")
        //        console.log(JSON.stringify(data))
        return JSON.stringify(data)
    }

    width: 380
    height: width * 16/9
    visible: true
    title: qsTr("HaQton")

    FontLoader { id: gameFont; source: "assets/fonts/PocketMonk-15ze.ttf" }
    Audio { source: "assets/audio/Magic-Clock-Shop_Looping.mp3"; loops: Audio.Infinite; autoPlay: true }

    QtObject {
        id: internal
        property int margins: 10
    }

    Rectangle {
        id: mainRect
        anchors.fill: parent
        color: "#44a8e4"

        SequentialAnimation on color {
            loops: Animation.Infinite
            ColorAnimation { to: "#44a8e4"; duration: 5000 } ColorAnimation { to: "#8bc740"; duration: 5000 }
            ColorAnimation { to: "#f3d034"; duration: 5000 } ColorAnimation { to: "#f79154"; duration: 5000 }
            ColorAnimation { to: "#f3d034"; duration: 5000 } ColorAnimation { to: "#8bc740"; duration: 5000 }
        }
    }

    StackView {
        id: stackViewId
        anchors { fill: parent; margins: internal.margins }
        initialItem: LoginScreen {}

        Component {
            id: ownerApprovePlayersId

            OwnerApprovePlayers {}
        }

        Component {
            id: playerApproveWaitId

            PlayerApproveWait {}
        }

        Component {
            id: homeScreen

            HomeScreen {}
        }
    }

    Dialog {
        id: createMatchDialogId
        margins: internal.margins

        width: parent.width - 20
        height: parent.height/3

        modal: true

        background: Rectangle {
            height: matchNameTextId.implicitHeight + matchNameTextFieldId.implicitHeight + createMatchButtonId.implicitHeight + 20
            color: "#44a8e4"

            SequentialAnimation on color {
                loops: Animation.Infinite
                ColorAnimation { to: "#44a8e4"; duration: 5000 } ColorAnimation { to: "#8bc740"; duration: 5000 }
                ColorAnimation { to: "#f3d034"; duration: 5000 } ColorAnimation { to: "#f79154"; duration: 5000 }
                ColorAnimation { to: "#f3d034"; duration: 5000 } ColorAnimation { to: "#8bc740"; duration: 5000 }
            }

            border.width: 2
            border.color: "white"

            ColumnLayout {
                anchors.centerIn: parent

                Label {
                    Layout.fillHeight: true; Layout.fillWidth: true
                    id: matchNameTextId
                    font { family: gameFont.name; pixelSize: 32 }
                    horizontalAlignment: Text.AlignHCenter
                    color: "white"; text: "Nome da Partida:"
                    height: 50
                }

                TextField {
                    Layout.fillHeight: true; Layout.fillWidth: true
                    id: matchNameTextFieldId
                    horizontalAlignment: Text.AlignHCenter
                    width: parent.width
                    font.pixelSize: 20
                    color: "white"
                    placeholderText: "NOME DA PARTIDA"
                    height: 50
                }

                Button {
                    Layout.fillHeight: true; Layout.fillWidth: true
                    id: createMatchButtonId
                    text: "CRIAR"
                    font.capitalization: Font.AllUppercase
                    Material.background: "white"
                    width: parent.width - 20
                    height: 50

                    onClicked: {
                        if (matchNameTextFieldId.text === "") {
                            console.log("Erro de nome de partida vazio")
                            return
                        }

                        createMatchDialogId.accept()
                    }
                }
            }
        }

        onAccepted: {
            mMatchName = matchNameTextFieldId.text

            let data = {}
            data.description = mMatchName
            data.nickname = mNickName
            data.email = mEmail

            console.log(JSON.stringify(data))

            create_match(data)
        }
    }

    Dialog {
        id: selectMatchDialogId

        width: parent.width - 20
        height: parent.height - 20
        margins: internal.margins
        modal: true

        background: Rectangle {
            anchors.fill: parent
            color: "#44a8e4"

            SequentialAnimation on color {
                loops: Animation.Infinite
                ColorAnimation { to: "#44a8e4"; duration: 5000 } ColorAnimation { to: "#8bc740"; duration: 5000 }
                ColorAnimation { to: "#f3d034"; duration: 5000 } ColorAnimation { to: "#f79154"; duration: 5000 }
                ColorAnimation { to: "#f3d034"; duration: 5000 } ColorAnimation { to: "#8bc740"; duration: 5000 }
            }

            border.width: 2
            border.color: "white"



            Label {
                anchors.top: parent.top
                anchors.topMargin: 10
                anchors.horizontalCenter: parent.horizontalCenter
                id: matchListTextId
                font { family: gameFont.name; pixelSize: 32 }
                horizontalAlignment: Text.AlignHCenter
                color: "white"; text: "Partidas"
                height: 50
            }

            ListView {
                id: listViewId
                anchors.top: matchListTextId.bottom
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.bottom: backButtonId.top
                anchors.margins: internal.margins
                model: matchesListModelId
                clip: true
                spacing: 5

                delegate: Rectangle {
                    id: rectId
                    width: listViewId.width
                    height: 80
                    color: mainRect.color
                    border.color: "white"
                    radius: 5

                    ColumnLayout {
                        id: columnLayoutId
                        anchors.fill: parent
                        anchors.margins: internal.margins
                        height: descriptionRowLayoutId.implicitHeight + statusRowLayoutId.implicitHeight + playersNumberRowLayoutId.implicitHeight + 20

                        RowLayout {
                            id: descriptionRowLayoutId
                            Text {
                                text: "DESCRICAO: "
                                color: "white"
                            }

                            Text {
                                text: description
                                color: "white"
                            }
                        }

                        RowLayout {
                            id: statusRowLayoutId
                            Text {
                                text: "STATUS: "
                                color: "white"
                            }

                            Text {
                                text: match_status_text(status)
                                color: "white"
                            }
                        }

                        RowLayout {
                            id: playersNumberRowLayoutId
                            Text {
                                text: "NUMERO DE PLAYERS: "
                                color: "white"
                            }

                            Text {
                                text: number_of_players
                                color: "white"
                            }
                        }
                    }

                    MouseArea {
                        anchors.fill: parent
                        hoverEnabled: true

                        onClicked: {
                            console.log("Selecionou a partida: "+description)
                            console.log("ID da partida: "+id)
                            console.log("Topico da partida: "+topic)
                            Core.setTopic(topic)
                            enter_match(id)
                            selectMatchDialogId.accept()
                        }

                        onEntered: {
                            rectId.color = "gray"
                        }

                        onExited: {
                            rectId.color = Qt.binding(function() {
                                return mainRect.color
                            })
                        }
                    }
                }

                ScrollBar.vertical: ScrollBar {}
            }

            Button {
                id: backButtonId

                anchors.horizontalCenter: parent.horizontalCenter
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 8

                width: listViewId.width
                height: 50

                text: "Voltar"

                onClicked: {
                    selectMatchDialogId.reject()
                }
            }
        }

        onAccepted: {
            stackViewId.push(playerApproveWaitId)
        }

        onRejected: {
            matchesListModelId.clear()
        }
    }

    ListModel {
        id: matchesListModelId
    }

    ListModel {
        id: mMatchListModelId
    }

    ListModel {
        id: playersListModelId
    }

    Component.onCompleted: {
        //Utilizado para sobrescrever em um topico
        //Core.setTopic("teste")
        //Dialog solicitando nickname e email
        //setCredentialsDialogId.open()

        console.log("QQDEu")
    }
}
