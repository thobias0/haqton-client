import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls.Material 2.15

import "FontAwesome"

Item {
    FontLoader { id: gameFont; source: "assets/fonts/PocketMonk-15ze.ttf" }

    ColumnLayout {
        width: parent.width
        anchors { verticalCenter: parent.verticalCenter }

        GridLayout {
            columns: 1; columnSpacing: internal.margins/2; rowSpacing: internal.margins/2

            Layout.fillWidth: true; Layout.fillHeight: false

            Label {
                id: greetingsLabel
                font { family: gameFont.name; pixelSize: 64 }
                Layout.alignment: Qt.AlignHCenter
                color: "white"; text: "Bem Vindo!"
            }

            Label {
                id: nickLabel
                text: "INSIRA SEU NICKNAME:"
                Layout.alignment: Qt.AlignHCenter
                color: "#1b1b1b"
                wrapMode: Text.WordWrap
            }

            TextField {
                id: nickTextFieldId
                Layout.fillWidth: true; Layout.fillHeight: true
                placeholderText: "Fulano"
                color: "#1b1b1b"
                horizontalAlignment: Text.AlignHCenter

            }

            Label {
                id: emailLabel
                text: "INSIRA SEU EMAIL"
                Layout.alignment: Qt.AlignHCenter
                color: "#1b1b1b"
                wrapMode: Text.WordWrap
            }

            TextField {
                id: emailTextFieldId
                Layout.fillWidth: true; Layout.fillHeight: true
                placeholderText: "fulano@ciclano.com"
                color: "#1b1b1b"
                horizontalAlignment: Text.AlignHCenter
                validator: RegExpValidator { regExp: /[A-Za-z0-9.]+@[A-Za-z0-9]+\.[A-Za-z]+\.([A-Za-z]+)?/ }

            }

            Button {
                Layout.fillWidth: true; Layout.fillHeight: true
                topInset: 0; bottomInset: 0
                font.capitalization: Font.AllUppercase

                Material.background: "white"

                text: "ENTRAR"

                onClicked: {
                    if (nickTextFieldId.text === "") {
                        console.log("Erro de nick vazio")
                        return
                    }

                    if (emailTextFieldId.text === "") {
                        console.log("Erro de email vazio")
                        return
                    }

                    mNickName = nickTextFieldId.text
                    mEmail = emailTextFieldId.text

                    stackViewId.replace(homeScreen)
                }
            }
        }
    }
}
