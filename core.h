/***************************************************************************
**
**  Copyright (C) 2020 by Qmob Solutions <contato@qmob.solutions>
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Foobar. If not, see <https://www.gnu.org/licenses/>.
**
***************************************************************************/

#ifndef CORE_H_
#define CORE_H_

#include <QObject>

class MessagingController;

class Core : public QObject
{
    Q_OBJECT
    Q_PROPERTY(MessagingController * messagingController READ messagingController CONSTANT)
    Q_PROPERTY(QString message READ message WRITE setMessage NOTIFY messageChanged)
    Q_PROPERTY(QString players READ players WRITE setPlayers NOTIFY playersChanged)
    Q_PROPERTY(QString myMatch READ myMatch WRITE setMyMatch NOTIFY myMatchChanged)
    //Q_PROPERTY(QString matches READ matches WRITE setMatches NOTIFY matchesChanged)

public:
    ~Core() Q_DECL_OVERRIDE;

    static Core *instance();

    MessagingController *messagingController() const;

    Q_INVOKABLE void setTopic(QString topic);
    Q_INVOKABLE void setNewPlayers(QString obj);
    Q_INVOKABLE void setMyNewMatch(QString obj);

    QString message() const;
    void setMessage(QString message);

    QString players() const;
    void setPlayers(QString players);

    QString myMatch() const;
    void setMyMatch(QString myMatch);

signals:
    void messageChanged(QString message);    
    void playersChanged(QString players);
    void myMatchChanged(QString myMatch);

private:
    explicit Core(QObject *parent = nullptr);

    static Core *_instance;
    MessagingController *_messagingController;
    QString m_message;
    QString m_players;
    QString m_myMatch;
};

#endif	// CORE_H_
