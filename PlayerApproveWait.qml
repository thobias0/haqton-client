import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls.Material 2.15
import solutions.qmob.haqton 1.0

import "FontAwesome"

Item {
    id: rootId
    //Strings que contem o valor da classe c++
    property string players: Core.players

    onPlayersChanged: {
        console.log("ATUALIZAR VALORES DOS PLAYERS")
        console.log(JSON.stringify(players))

        playersListModelId.clear()
        var obj = JSON.parse(players)

        let mApproveCounter = 0

        let playerBan = true
        Object.keys(obj).forEach(function(key) {
            playersListModelId.append(obj[key])
            if(obj[key].id === mId) {
                console.log("Encontrou o id")
                playerBan = false
            }
        })

        if(playerBan) {
            console.log("\n\n\n")
            console.log("EXPULSO")
            //Retorna usuario a tela inicial
            stackViewId.pop()
            console.log("\n\n\n")
        }

        console.log("playersListModelId.count: "+playersListModelId.count)
    }

    Rectangle {
        id: containerRectId
        width: parent.width; height: parent.height
        color: "#44a8e4"
        radius: 15

        SequentialAnimation on color {
            loops: Animation.Infinite
            ColorAnimation { to: "#44a8e4"; duration: 5000 } ColorAnimation { to: "#8bc740"; duration: 5000 }
            ColorAnimation { to: "#f3d034"; duration: 5000 } ColorAnimation { to: "#f79154"; duration: 5000 }
            ColorAnimation { to: "#f3d034"; duration: 5000 } ColorAnimation { to: "#8bc740"; duration: 5000 }
        }

        ListView {
            id: listViewId
            anchors.top: parent.top
            anchors.topMargin: 8
            anchors.left: parent.left
            anchors.leftMargin: 8
            anchors.right: parent.right
            anchors.rightMargin: 8
            anchors.bottom: buttonsContainerId.top
            anchors.bottomMargin: 8
            model: playersListModelId
            clip: true
            spacing: 5
            headerPositioning: ListView.OverlayHeader
            footerPositioning: ListView.OverlayFooter

            delegate: Rectangle {
                id: rectId
                width: listViewId.width
                height: 110
                color: containerRectId.color
                border.color: "white"
                radius: 5

                ColumnLayout {
                    anchors.top: parent.top
                    anchors.left: parent.left
                    anchors.bottom: parent.bottom
                    width: parent.width/2
                    anchors.margins: 8

                    RowLayout {
                        Layout.fillHeight: true
                        Text {
                            Layout.fillHeight: true
                            text: "NOME: "
                            color: "white"
                        }

                        Text {
                            Layout.fillHeight: true
                            text: player_name
                            color: "white"
                        }
                    }

                    RowLayout {
                        Layout.fillHeight: true
                        Text {
                            Layout.fillHeight: true
                            text: "STATUS: "
                            color: "white"
                        }

                        Text {
                            Layout.fillHeight: true
                            text: approved ? "APROVADO" : "AGUARDANDO"
                            color: "white"
                        }
                    }

                    RowLayout {
                        Layout.fillHeight: true
                        Text {
                            Layout.fillHeight: true
                            text: "E-MAIL: "
                            color: "white"
                        }

                        Text {
                            Layout.fillHeight: true
                            text: player_email
                            color: "white"
                        }
                    }
                }
            }

            header: Rectangle {
                id: playersListTextContainerId
                width: parent.width
                height: 50
                color: containerRectId.color
                z: 3

                Label {
                    anchors.centerIn: parent
                    horizontalAlignment: Text.AlignHCenter
                    id: matchNameTextId
                    font { family: gameFont.name; pixelSize: 32 }
                    color: "white"; text: "JOGADORES"
                    height: 50
                }

                MouseArea {
                    anchors.fill: parent

                    onClicked: {

                    }
                }
            }
        }

        Rectangle {
            id: buttonsContainerId
            anchors.bottom: parent.bottom
            width: parent.width
            height: 50
            color: containerRectId.color
            z: 3

            Button {
                anchors.centerIn: parent
                text: "VOLTAR"
                width: listViewId.width

                onClicked:  {
                    stackViewId.pop()
                }
            }

        }
    }

    ListModel {
        id: playersListModelId
    }
}
